import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class VariaveisGlobaisProvider {

  private usuario : String;
  private senha : String;
  private estaLogado : Boolean;
  
  constructor() {
    this.usuario = null;
    this.senha = null;
    this.estaLogado = false;
  }

  public getUsuario() : String {

    return this.usuario;

  }

  public setUsuario(usuario : String){
        
    this.usuario = usuario;
  }

  public getSenha() : String {

    return this.senha;

  }

  public setSenha(senha : String){
        
    this.senha = senha;
  }
  public getEstaLogado () : Boolean{

    return this.estaLogado;
  }

  public setEstaLogado (estaLogado : Boolean) {

      this.estaLogado = estaLogado;

  }
}
