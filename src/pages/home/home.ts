import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { VariaveisGlobaisProvider } from '../../providers/variaveis-globais/variaveis-globais';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  private bandas: Array<Object> = [];

  constructor(private variaveisGlobais : VariaveisGlobaisProvider) {

    this.bandas = [
      {
        "bando": "Lote 7",
        "genero": "Rock",
        "site": "http://www.bandalote7.com.br/",
        "img": "http://toquenobrasil-uploads.s3.amazonaws.com/wp-content/uploads/2018/04/lote-8633-2-5ae1d7ad9288f-898x600.jpg"
      },
      {
        "bando": "Guns N Rose",
        "genero": "Rock",
        "site": "https://www.gunsnroses.com/",
        "img": "http://gunsnrosesbrasil.com/wp/wp-content/uploads/2014/04/gunsnroses-80-classic.jpg"

      },
      {
        "bando": "Aviões do Forró",
        "genero": "Forró",
        "site": "http://xandaviao.com.br/",
        "img": "https://i.ytimg.com/vi/GdVAOfOVkUk/maxresdefault.jpg"
      },
      {
        "bando": "MC Guime",
        "genero": "Funk",
        "site": "https://pt.wikipedia.org/wiki/MC_Guim%C3%AA",
        "img": "https://www.obaoba.com.br/contentFiles/system/pictures/2014/5/281851/original/mc-guime.jpg"
      },
      {
        "bando": "Wesley Safadão",
        "genero": "Sertanejo",
        "site": "https://www.wesleysafadao.com.br/",
        "img": "https://i.ytimg.com/vi/-LfIaPpEe1Y/maxresdefault.jpg"
      }
    ]

  }

}
